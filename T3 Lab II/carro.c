#include <string.h>
#include <stdlib.h>
#include "carro.h"
#include "io.h"

void CarroInsereAndRead(DataBaseCarros* db) {

	char* placa = ReadString("Placa do carro: ");
	char* marca = ReadString("Marca do carro: ");
	int ano = ReadInt("Ano do carro: ");

	CarroInsereWithInfo(db, placa, marca, ano);
}

void CarroInsereWithInfo(DataBaseCarros* db, char* placa, char* marca, int ano) {
	Carro* carro = malloc(sizeof(Carro));

	carro->placa = placa;
	carro->marca = marca;
	carro->ano = ano;

	CarroInsere(carro, db);
}

void CarroInsere(Carro* carro, DataBaseCarros* db) {
	db->arvorePlaca = ArvoreInsereOrdenado(carro, db->arvorePlaca, CarroComparaPlaca);
	db->arvoreMarca = ArvoreInsereOrdenado(carro, db->arvoreMarca, CarroComparaMarca);
	db->arvoreAno = ArvoreInsereOrdenado(carro, db->arvoreAno, CarroComparaAno);
	db->lista = ListaInsereOrdenado(carro, db->lista, CarroComparaPlaca);
}

int CarroComparaPlaca(Carro* a, Carro* b) { return strcmp(a->placa, b->placa); }
int CarroComparaMarca(Carro* a, Carro* b) { return strcmp(a->marca, b->marca); }
int CarroComparaAno(Carro* a, Carro* b) { return  a->ano - b->ano; }

void CarroRemove(DataBaseCarros* db) {
	char* placa = ReadString("Placa do carro para remover: ");
	Carro* carro = CarroBusca(placa, db->arvorePlaca);

	if (carro == NULL) {
		PrintLine("Carro n�o encontrado");
		free(placa);
		return;
	}

	db->arvorePlaca = ArvoreRemove(carro, db->arvorePlaca, CarroComparaPlaca);
	db->arvoreMarca = ArvoreRemove(carro, db->arvoreMarca, CarroComparaMarca);
	db->arvoreAno = ArvoreRemove(carro, db->arvoreAno, CarroComparaAno);
	db->lista = ListaRemove(carro, db->lista);

	//CarroFree(carro);
	free(placa);
}

void CarroListarLista(Lista* lista) {
	for (Lista* l = lista; l != NULL; l = l->next)
		PrintCarro(l->carro);
}

void CarroListarArvore(Arvore* arvore) {
	if (arvore == NULL)
		return;

	CarroListarArvore(arvore->l);
	PrintCarro(arvore->carro);
	CarroListarArvore(arvore->r);
}

Carro* CarroBusca(char* placa, Arvore* arvorePlaca) {
	if (arvorePlaca == NULL || placa == NULL)
		return NULL;

	int cmp = strcmp(placa, arvorePlaca->carro->placa);

	if (cmp == 0)
		return arvorePlaca->carro;
	else if (cmp > 0)
		return CarroBusca(placa, arvorePlaca->r);
	else
		return CarroBusca(placa, arvorePlaca->l);
}

void CarroFree(Carro* c) {
	free(c->placa);
	free(c->marca);
	free(c);
}