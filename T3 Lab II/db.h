#pragma once

typedef struct {
	struct lista* lista;
	struct arvore* arvorePlaca;
	struct arvore* arvoreMarca;
	struct arvore* arvoreAno;
} DataBaseCarros;

DataBaseCarros* CriaDBCarros();