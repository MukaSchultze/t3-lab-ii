#pragma once
#include "carro.h"

typedef struct lista {
	Carro* carro;

	struct lista* next;
	struct lista* prev;

} Lista;

Lista* ListaCria();
Lista* ListaInsereOrdenado(Carro* carro, Lista* lista, int(*cmp)(Carro*, Carro*));
Lista* ListaRemove(Carro* c, Lista* l);
void ListaFree(Lista* l);