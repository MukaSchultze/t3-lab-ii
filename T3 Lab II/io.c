#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"

void* Clone(void* src, int length) {
	char* dst = malloc(length);

	for (int i = 0; i < length; i++)
		dst[i] = ((char*)src)[i];

	return dst;
}

char* CloneString(char* src) {
	return Clone(src, strlen(src) + 1);
}

void PrintLine(char* str) {
	printf("%s\n", str);
}

void PrintCarro(Carro* carro) {
	printf("%s - %d   %s\n", carro->placa, carro->ano, carro->marca);
}

int ReadInt(char* label) {
	int result;
	printf("%s", label);
	scanf("%d", &result);
	return result;
}

float ReadFloat(char* label) {
	float result;
	printf("%s", label);
	scanf("%f", &result);
	return result;
}

char* ReadString(char* label) {
	char str[1024];
	printf("%s", label);

	str[0] = getchar();

	if (str[0] == '\n' || str[0] == '\0')
		fgets(str, sizeof(str), stdin);
	else
		fgets(str + 1, sizeof(str) - 1, stdin);

	str[strlen(str) - 1] = '\0';

	return CloneString(str);
}
