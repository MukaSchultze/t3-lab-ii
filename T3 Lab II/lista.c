#include <stdlib.h>
#include "lista.h"

Lista* ListaCria() {
	return NULL;
}

Lista* ListaInsereOrdenado(Carro* carro, Lista* lista, int(*cmp)(Carro*, Carro*)) {
	Lista* novo = malloc(sizeof(Lista));

	novo->next = NULL;
	novo->prev = NULL;
	novo->carro = carro;

	if (lista == NULL)
		return novo;

	Lista* insertAt = NULL;

	for (Lista* l = lista; l != NULL; l = l->next) {
		int c = cmp(carro, l->carro);

		if (c < 0)
			break;
		else
			insertAt = l;
	}

	if (insertAt == NULL) {
		novo->next = lista;
		if (lista != NULL)
			lista->prev = novo;
		return novo;
	}
	else {
		novo->next = insertAt->next;
		novo->prev = insertAt;

		if (insertAt->next != NULL)
			insertAt->next->prev = novo;

		insertAt->next = novo;
		return lista;
	}
}

Lista* ListaRemove(Carro* c, Lista* l) {
	for (Lista* i = l; i != NULL; i = i->next) {
		if (i->carro != c)
			continue;

		if (i == l)
			l = l->next;

		if (i->prev != NULL)
			i->prev->next = i->next;
		if (i->next != NULL)
			i->next->prev = i->prev;

		free(i);
		return l;
	}

	return l;
}

void ListaFree(Lista* l) {
	while (l != NULL) {
		if (l->carro != NULL) {
			free(l->carro->marca);
			free(l->carro->placa);
			free(l->carro);
		}

		Lista* aux = l;
		l = l->next;
		free(aux);
	}
}