#pragma once
#include "carro.h"

void* Clone(void* src, int length);
char* CloneString(char* src);

void PrintLine(char* str);
void PrintCarro(Carro* carro);

int ReadInt(char* label);
float ReadFloat(char* label);
char* ReadString(char* label);