#pragma once
#include "carro.h"

typedef struct arvore {
	Carro* carro;

	struct arvore* l;
	struct arvore* r;

} Arvore;

Arvore* ArvoreCria();
Arvore* ArvoreInsereOrdenado(Carro* c, Arvore* arvore, int(*cmp)(Carro*, Carro*));
Arvore* ArvoreInsereOrdenadoNode(Arvore* novo, Arvore* arvore, int(*cmp)(Carro*, Carro*));
Arvore* ArvoreRemove(Carro* carro, Arvore* a, int(*cmp)(Carro*, Carro*));
void ArvoreFree(Arvore* a);
