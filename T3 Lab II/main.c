#include <stdlib.h>
#include "db.h"
#include "arvore.h"
#include "lista.h"
#include "carro.h"
#include "io.h"

/*
* Samuel Schultze - SI - 13/06/2018 - Lab II
* gcc *.c -Wall
*/

void BuscarCarroMsg(Arvore* arvorePlaca) {
	char* placa = ReadString("Placa do carro para buscar: ");

	Carro* carro = CarroBusca(placa, arvorePlaca);

	if (carro == NULL)
		PrintLine("Placa n�o encontrada");
	else
		PrintCarro(carro);

	free(placa);
}

int main() {

	DataBaseCarros* db = CriaDBCarros();

	CarroInsereWithInfo(db, "ABC1256", "Ford", 1998);
	CarroInsereWithInfo(db, "IUR1560", "VW", 2016);
	CarroInsereWithInfo(db, "NHI0235", "Ferrari", 2005);
	CarroInsereWithInfo(db, "JTR0213", "Fiat", 2005);
	CarroInsereWithInfo(db, "QNK1510", "Ford", 1975);
	CarroInsereWithInfo(db, "DEF7415", "Mazda", 2004);
	CarroInsereWithInfo(db, "BFA8188", "Azura", 2010);
	CarroInsereWithInfo(db, "NJK6405", "Lambo", 2018);

	while (1) {

		PrintLine("\n-----------------\n");
		PrintLine("0 - Sair");
		PrintLine("1 - Inserir");
		PrintLine("2 - Remover");
		PrintLine("3 - Buscar");
		PrintLine("4 - Listar - Placas");
		PrintLine("5 - Listar - Marca");
		PrintLine("6 - Listar - Ano");
		PrintLine("7 - Listar - Placas (�rvore)");

		int opt = ReadInt("\nInsira a op��o: ");

		switch (opt) {
			case 0: return 0;
			case 1: CarroInsereAndRead(db); break;
			case 2: CarroRemove(db); break;
			case 3: BuscarCarroMsg(db->arvorePlaca); break;
			case 4: CarroListarLista(db->lista); break;
			case 5: CarroListarArvore(db->arvoreMarca); break;
			case 6: CarroListarArvore(db->arvoreAno); break;
			case 7: CarroListarArvore(db->arvorePlaca); break;
			default: PrintLine("Op��o inv�lida");
		}
	}

}