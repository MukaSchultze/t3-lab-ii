#include <stdlib.h>
#include "arvore.h"

Arvore* ArvoreCria() {
	return NULL;
}

Arvore* ArvoreInsereOrdenado(Carro* c, Arvore* arvore, int(*cmp)(Carro*, Carro*)) {
	Arvore* novo = malloc(sizeof(Arvore));

	novo->carro = c;
	novo->l = NULL;
	novo->r = NULL;

	return	ArvoreInsereOrdenadoNode(novo, arvore, cmp);
}

Arvore* ArvoreInsereOrdenadoNode(Arvore* novo, Arvore* arvore, int(*cmp)(Carro*, Carro*)) {
	if (arvore == NULL)
		return novo;

	int c = cmp(novo->carro, arvore->carro);

	if (c < 0)
		if (arvore->l != NULL)
			ArvoreInsereOrdenadoNode(novo, arvore->l, cmp);
		else
			arvore->l = novo;

	else
		if (arvore->r != NULL)
			ArvoreInsereOrdenadoNode(novo, arvore->r, cmp);
		else
			arvore->r = novo;

	return arvore;
}

Arvore* ArvoreRemove(Carro* carro, Arvore* a, int(*cmp)(Carro*, Carro*)) {
	if (a == NULL)
		return NULL;

	int c = cmp(carro, a->carro);

	if (c < 0)
		a->l = ArvoreRemove(carro, a->l, cmp);
	else if (c > 0)
		a->r = ArvoreRemove(carro, a->r, cmp);

	else {
		if (a->l == NULL && a->r == NULL) {
			free(a);
			return NULL;
		}
		else if (a->l == NULL) {
			Arvore* t = a;
			a = a->r;
			free(t);
		}
		else if (a->r == NULL) {
			Arvore* t = a;
			a = a->l;
			free(t);
		}
		else {
			Arvore* f = a->l;

			while (f->r != NULL)
				f = f->r;

			a->carro = f->carro;
			f->carro = carro;
			a->l = ArvoreRemove(carro, a->l, cmp);
		}
	}

	return a;
}

void ArvoreFree(Arvore* a) {
	if (a == NULL)
		return;

	ArvoreFree(a->l);
	ArvoreFree(a->r);
	free(a);
}