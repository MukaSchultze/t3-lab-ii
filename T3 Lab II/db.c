#include <stdlib.h>
#include "db.h"

DataBaseCarros* CriaDBCarros() {
	DataBaseCarros* db = malloc(sizeof(DataBaseCarros));

	db->lista = NULL;
	db->arvoreAno = NULL;
	db->arvoreMarca = NULL;
	db->arvorePlaca = NULL;

	return db;
}