#pragma once
#include "db.h"

typedef struct {
	char* placa;
	char* marca;
	int ano;
} Carro;

typedef struct arvore Arvore;
typedef struct lista Lista;

#include "lista.h"
#include "arvore.h"

void CarroInsereAndRead(DataBaseCarros* db);
void CarroInsereWithInfo(DataBaseCarros* db, char* placa, char* marca, int ano);
void CarroInsere(Carro* carro, DataBaseCarros* db);

void CarroRemove(DataBaseCarros* db);

int CarroComparaPlaca(Carro* a, Carro* b);
int CarroComparaMarca(Carro* a, Carro* b);
int CarroComparaAno(Carro* a, Carro* b);

void CarroListarLista(Lista* lista);
void CarroListarArvore(Arvore* arvore);

Carro* CarroBusca(char* placa, Arvore* arvorePlaca);

void CarroFree(Carro* c);
